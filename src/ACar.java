/**
 * Common workflow in java:
 * 
 * Create an interface (let's us program in general)
 * Create an abstract class (let's us code shared functionality)
 * Create concrete classes
 * 
 * Abstract:
 * -Cannot be instantiated
 * -Concrete classes that inherit from the abstract class *must* implement
 * all the abstract methods
 * -Abstract subclasses may or may not implement the abstract methods
 * 
 * implements means I'm inheriting from an interface
 * You can implement as many interfaces as you want
 * 
 * Everything extends object
 * 
 * @author unouser
 *
 */


public abstract class ACar implements ICar, java.io.Serializable{
	
	/** The make car */
	private String make;
	
	/** The model of the car */
	private String model;
	
	/** The year of the car */
	private int year;
	
	/** The mileage of the car */
	private int mileage;
	
	
	/**
	 * The contructor for an abstract car
	 * @param inMake The make of the car
	 * @param inModel The model of the car
	 * @param inYear The year of the car
	 */
	public ACar(String inMake, String inModel, int inYear) {
		make = inMake;
		model = inModel;
		year = inYear;
	}
	
	protected void setMileage(int inMileage)
	{
		mileage = inMileage;
	}
	
	@Override
	public String getMake()
	{
		return make;
	}
	
	@Override
	public String getModel() {
		return model;
	}
	
	@Override
	public int getMileage() {
		return mileage;
	}
	
	@Override
	public int getYear() {
		return year;
	}
	
	@Override
	public String toString() {
		return getMake() + " " + getModel();
	}

	
	
	
}
